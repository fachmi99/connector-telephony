# Flectra Community / connector-telephony





Available addons
----------------

addon | version | summary
--- | --- | ---
[crm_phone](crm_phone/) | 1.0.1.0.0| Validate phone numbers in CRM
[base_phone_popup](base_phone_popup/) | 1.0.1.0.0| Pop-up the related form view to the user on incoming calls
[ovh_telephony_connector](ovh_telephony_connector/) | 1.0.0.1.0| OVH-Odoo telephony connector (click2call)
[base_phone](base_phone/) | 1.0.1.0.0| Validate phone numbers
[hr_recruitment_phone](hr_recruitment_phone/) | 1.0.1.0.0| Validate phone numbers in HR Recruitment
[base_sms_client](base_sms_client/) | 1.0.1.0.0| Sending SMSs very easily, individually or collectively.
[event_phone](event_phone/) | 1.0.1.0.0| Validate phone numbers in Events
[hr_phone](hr_phone/) | 1.0.1.0.0| Validate phone numbers in HR
[sms_send_picking](sms_send_picking/) | 1.0.1.0.0| Sms Send Picking
[asterisk_click2dial](asterisk_click2dial/) | 1.0.1.0.0| Asterisk-Odoo connector
[ovh_sms_client](ovh_sms_client/) | 1.0.1.0.1| OVH SMS Client


